import prettierConfigBase from '@boojum/prettier-config-base'

export default { ...prettierConfigBase }
