import eslintConfigJsWeb from '@boojum/eslint-config-js-web'
import gitignore from 'eslint-config-flat-gitignore'

export default [...eslintConfigJsWeb, gitignore()]
