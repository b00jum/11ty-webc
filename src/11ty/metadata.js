module.exports = {
  title: '11ty + WebC',
  description: '11ty + WebC Base Template',
  language: 'en',
  url: process.env['NODE_ENV'] === 'development' ? 'http://localhost:8080' : 'https://example.com',
}
