<!doctype html>
<html :lang="metadata.language">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title @text="title || metadata.title"></title>
    <meta name="description" :content="description || metadata.description" />
    <meta name="color-scheme" content="light dark" />
    <meta name="theme-color" content="#fbf1c7" media="(prefers-color-scheme: light)" />
    <meta name="theme-color" content="#282828" media="(prefers-color-scheme: dark)" />
    <link rel="canonical" :href="metadata.url + page.url" />
    <link rel="manifest" href="/manifest.json" />
    <link rel="icon" href="/favicon.ico" sizes="any" />
    <link rel="icon" href="/favicon.svg" type="image/svg+xml" />
    <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
    <meta name="generator" :content="eleventy.generator" />

    <link rel="stylesheet" href="../../assets/css/index.css" />
    <style webc:keep @raw="getBundle('css')"></style>
  </head>

  <body>
    <site-header>
      <brand slot="brand" :name="metadata.title"></brand>
      <theme-toggle slot="toggle"></theme-toggle>
    </site-header>

    <main @raw="content"></main>
  </body>
</html>
