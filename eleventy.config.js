const eleventyWebcPlugin = require('@11ty/eleventy-plugin-webc')
const eleventyBundlerPlugin = require('@11ty/eleventy-plugin-bundle')
const { browserslistToTargets, transform } = require('lightningcss')
const browserslist = require('browserslist')

const targets = browserslistToTargets(browserslist('> 0.2% and not dead'))
const isProd = process.env['NODE_ENV'] === 'production'

module.exports = function (config) {
  config.setQuietMode(true)

  config.addPlugin(eleventyWebcPlugin, { components: 'src/11ty/components/**/*.webc' })

  config.addPassthroughCopy({ 'src/assets/*.*': '.' })

  config.addPlugin(eleventyBundlerPlugin, {
    transforms: [
      function (content) {
        if (this.type === 'css') {
          let { code } = transform({
            code: Buffer.from(content),
            minify: isProd,
            sourceMap: !isProd,
            targets,
            drafts: {
              nesting: true,
            },
          })
          return code
        }

        return content
      },
    ],
  })

  return {
    dir: {
      output: 'dist',
      input: 'src',
      data: '11ty',
      includes: '11ty',
      layouts: '11ty/layouts',
    },
  }
}
